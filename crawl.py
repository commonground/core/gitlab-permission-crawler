import os
from prettytable import PrettyTable
import requests

MAIN_GROUP_ID = '3094865' # commonground group id

ACCESS_LEVELS = {
    0: 'No access',
    5: 'Minimal access',
    10: 'Guest',
    20: 'Reporter',
    30: 'Developer',
    40: 'Maintainer',
    50: 'Owner'
}

headers = {
    'PRIVATE-TOKEN': os.getenv('GITLAB_PRIVATE_TOKEN')
}

groups = []
projects = []

matrix = []

# Add main group
groups.append(requests.get('https://gitlab.com/api/v4/groups/{}'.format(MAIN_GROUP_ID), headers=headers).json())

# Fetch all descendant groups
descendant_groups = requests.get(f'https://gitlab.com/api/v4/groups/{MAIN_GROUP_ID}/descendant_groups?per_page=100', headers=headers).json()
for descendant_group in descendant_groups:
    groups.append(descendant_group)

# Fetch all projects
for group in groups:
    print('Fetching projects of {}'.format(group['name']))
    for project in requests.get('https://gitlab.com/api/v4/groups/{}/projects?per_page=100'.format(group['id']), headers=headers).json():
        projects.append(project)

print('Fetched {} groups and {} projects'.format(len(groups), len(projects)))

table = PrettyTable()
table.field_names = ['Username', 'Name', 'Access level', 'Group / project']

for group in groups:
    print('Fetching members of {}'.format(group['name']))
    members = requests.get('https://gitlab.com/api/v4/groups/{}/members?per_page=100'.format(group['id']), headers=headers).json()
    for member in members:
        table.add_row([
            member['username'],
            member['name'],
            ACCESS_LEVELS[member['access_level']],
            'Group: {}'.format(group['name'])
        ])

for project in projects:
    print('Fetching members of {}'.format(project['name']))
    members = requests.get('https://gitlab.com/api/v4/projects/{}/members?per_page=100'.format(project['id']), headers=headers).json()
    for member in members:
        table.add_row([
            member['username'],
            member['name'],
            ACCESS_LEVELS[member['access_level']],
            'Project: {}'.format(project['name'])
        ])

print(table.get_string(sortby='Username'))
