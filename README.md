# Permission crawler

Iterate through all descendant groups and projects on GitLab and create an overview of all assigned access levels within a group

## Use

Create a virtual environment with:

```bash
$ python3 -m venv .env
$ source .env/bin/activate
```

Install required Python dependencies with:

```bash
$ pip3 install -r requirements.txt
```

Fetch a [personal access token from GitLab](https://gitlab.com/-/profile/personal_access_tokens) with the permission `read_api` and make it available in your environment with:

```bash
$ source GITLAB_PRIVATE_TOKEN=<token>
```

The run the script:

```bash
$ python crawl.py

+----------------------+----------------------+--------------+--------------------------------------------------+
|       Username       |         Name         | Access level |                 Group / project                  |
+----------------------+----------------------+--------------+--------------------------------------------------+
|    *************     |    *************     |    Guest     |              Group: Huishoudboekje               |
|        ******        |        ******        |   Reporter   |    Group: Common Ground Componenten­catalogus     |
|       *******        |    *************     |    Guest     |              Group: Huishoudboekje               |
|      *********       |    *************     |  Developer   |              Project: Architectuur               |
+----------------------+----------------------+--------------+--------------------------------------------------+
```

The script will output a table with all permissions granted on group and project level.
